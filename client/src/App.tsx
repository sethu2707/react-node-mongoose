import React from 'react';
import { Layout } from 'antd'
import ErrorBoundary from './components/ErrorBoundary'
import { GlobalLoadingDecoratorProvider } from './contexts/GlobalLoaderContext'
import GlobalLoader from './components/GlobalLoader'
import Router from './routes/Router'
import Header from './components/Header'
const { Content } = Layout

const App: React.FC = () => {
  return (
    <ErrorBoundary>
      <GlobalLoadingDecoratorProvider>
        <GlobalLoader/>
        <Layout>
          <Header/>
          <Content>
            <div>
              <Router />
            </div>
          </Content>
        </Layout>
      </GlobalLoadingDecoratorProvider>
    </ErrorBoundary>
  )
}

export default App;
