import * as React from 'react'
import { Col, Form } from 'antd'

interface Props {
    label?: string | JSX.Element
    highlight?: boolean
    span: number
}

const BatchFormLabel: React.FC<Props> = ({ span, label, highlight, children }) => {
    return (
        <Col span={span}>
            <div className="form-label">
                <div className="form-label-txt">{label}</div>
            </div>
            <Form.Item noStyle={true}>{children as any}</Form.Item>
        </Col>
    )
}


export default BatchFormLabel
