import React from 'react'
import { Spin } from 'antd'
import { useGlobalLoadingContext } from '../contexts/GlobalLoaderContext'
import '../App.css'
import 'antd/dist/antd.css';

const GlobalLoader: React.FC = () => {
    const { displayLoader } = useGlobalLoadingContext()
    return displayLoader ? (
        <div className="global-loader" >
            <Spin size="large" />
        </div>
    ) : null
}

export default GlobalLoader
