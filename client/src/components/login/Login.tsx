import * as React from 'react'
import LoginForm from '../forms/Login/LoginForm'
import { useGlobalLoadingContext } from '../../contexts/GlobalLoaderContext'
import { login, CreateUserBody } from '../../services/login.service'
import { notification } from 'antd'
import { Redirect } from 'react-router-dom'

interface Props { }

const Login: React.FC<Props> = () => {
    const { toggleLoadingDecorator } = useGlobalLoadingContext()
    const [loggedin, setloggedin] = React.useState(false)

    React.useEffect(() => {
        toggleLoadingDecorator(false)
    }, [])

    const onFinish = async (data: CreateUserBody) => {
        toggleLoadingDecorator(true)
        let response: any = await login(data)
        if (response.success) {
            toggleLoadingDecorator(false)
            let data: any = {}
            data.id = response?.data?._id
            data.token= response.token
            sessionStorage.setItem('user', JSON.stringify(data))
            openNotification('success', 'Successfully login')
            setloggedin(true)
        } else {
            openErrorNotification('error', 'Please try again later')
        }
    }

    const openNotification = (type: 'success', des: any) => {
        notification[type]({
            message: type,
            description: des,
        });
    };
    const openErrorNotification = (type: 'error', des: any) => {
        notification[type]({
            message: type,
            description: des,
        });
    };

    if (loggedin) {
        return <Redirect to="/profile" />
    }
    return (
        <div className="global-loader" style={{ zIndex: 'unset' }}>
            <LoginForm onFinish={(e: CreateUserBody) => onFinish(e)} />
        </div>
    )
}
export default Login
