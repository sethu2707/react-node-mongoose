import React, { useEffect, useState } from 'react'
import { Row, Form, Input, Card, Button, Select, DatePicker } from 'antd'
import { UserOutlined } from '@ant-design/icons';
import BatchFormLabel from '../../BatchFormLabel'
import { userData } from '../../profile/Profile'
import { useForm, Controller } from 'react-hook-form'
import moment from "moment";

interface Props {
    onFinish: (data: userData) => void
    res?: userData
}

const ProfileForm: React.FC<Props> = ({ onFinish, res }) => {
    const [input, setInput] = useState<userData>({})
    const [date, setDate] = useState<any>(null)
    const [gender, setGender] = useState("")


    useEffect(() => {
        if (res) {
            let apiData = {
                email: res?.email,
                firstName: res?.firstName,
                lastName: res?.lastName,
                address: res?.address
            }
            setDate(res?.dob || null)
            setGender(res?.gender || '')
            // setInput(apiData)

        }
    }, [res])

    const { handleSubmit, formState, control, watch, trigger } = useForm<userData>({
        mode: 'onBlur',
        defaultValues: {
            firstName: res?.firstName,
            lastName: res?.lastName,
            email: res?.email,
            address: res?.address,
        }
    })

    const isValid = input.email && input.address && input.firstName &&
        input.lastName && input.address&& gender


    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const onChange = (e: any) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

 

    const updateUserData = () => {
        let user: userData = {}
        user.address = input.address;
        user.firstName = input.firstName;
        user.lastName = input.lastName;
        user.email = input.email;
        user.phoneNumber = input.phoneNumber;
        user.gender = gender;
        user.dob = moment(date).format("DD/MM/YYYY");
        onFinish(user)
    }

    console.log("userData", moment(date).format("DD/MM/YYYY"),date)
    
    const dateFormat = 'YYYY/MM/DD';
    return (
        <Card className="user-form">
            {console.log("data-->",input)}
            <Form
                name="user_login"
                initialValues={{
                    remember: true,
                    
                }}
                onFinish={() => updateUserData()}
                onFinishFailed={onFinishFailed}
            >
                <Row >
                    <BatchFormLabel label="User Email" span={24}>
                        <Form.Item
                            rules={[{ required: true, message: 'Please enter your email!' }]}
                        >
                            <Input value={input.email} name="email" prefix={<UserOutlined />} placeholder="Email" onChange={(e: any) => { onChange(e) }} />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Row >
                    <BatchFormLabel label="FirstName" span={24}>
                        <Form.Item
                            rules={[{ required: true, message: 'Please enter firstName!' }]}
                        >
                            <Input value={input.firstName} name="firstName" prefix={<UserOutlined />} placeholder="FirstName" onChange={(e: any) => { onChange(e) }} />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Row >
                    <BatchFormLabel label="LastName" span={24}>
                        <Form.Item
                            rules={[{ required: true, message: 'Please enter LastName!' }]}
                        >
                            <Input value={input.lastName} name="lastName" prefix={<UserOutlined />} placeholder="LastName" onChange={(e: any) => { onChange(e) }} />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Row >
                    <BatchFormLabel label="Gender" span={24}>
                        <Form.Item
                            rules={[{ required: true, message: 'Please select Gender!' }]}
                        >

                            <Select onChange={(e: any) => { setGender(e) }} placeholder="Select" value={gender}>
                                <Select.Option value="Male">Male</Select.Option>
                                <Select.Option value="Female">Female</Select.Option>
                            </Select>
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Row >
                    <BatchFormLabel label="Date of Brith" span={24}>
                        <Form.Item
                            rules={[{ required: true, message: 'Please select Date of Birth!' }]}
                        >
                            <DatePicker style={{ width: '100%' }} value={date} placeholder={date ? date : "Date"} onChange={(e: any) => { setDate(e) }} format={dateFormat} />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Row >
                    <BatchFormLabel label="Address" span={24}>
                        <Form.Item
                            rules={[{ required: true, message: 'Please select Address!' }]}
                        >
                            <Controller
                                control={control}
                                name="address"
                                render={({ field }) => (
                                    <Input
                                        aria-label="address"
                                        value={field.value}
                                        onChange={(e)=>{
                                            field.onChange(e);
                                            trigger('address')
                                        }}
                                        onBlur={field.onBlur}
                                    />
                                )}
                            />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Form.Item>
                    <Button type="primary" htmlType="submit" disabled={!isValid}>
                        Update
                    </Button>
                </Form.Item>
            </Form>
        </Card>
    )
}
export default ProfileForm