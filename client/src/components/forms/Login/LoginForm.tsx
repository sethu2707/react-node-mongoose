import React, { useState } from 'react'
import { Row, Form, Input, Card, Button } from 'antd'
import BatchFormLabel from '../../BatchFormLabel'
import { UserOutlined } from '@ant-design/icons';
import { CreateUserBody } from '../../../services/login.service'

interface Props {
    onFinish: (data: CreateUserBody) => void
}

const LoginForm: React.FC<Props> = ({ onFinish }) => {
    const [userEmail, setuserEmail] = useState('')

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <Card>
            <Form
                name="user_login"
                initialValues={{
                    remember: true,
                }}
                onFinish={() => onFinish({ email: userEmail, password: 'Admin' })}
                onFinishFailed={onFinishFailed}
            >
                <Row >
                    <BatchFormLabel label="User Email" span={24}>
                        <Form.Item
                            name="email"
                            rules={[{
                                type: 'email',
                                message: 'Please enter valid E-mail!',
                            }, { required: true, message: 'Please enter your email!' }]}
                        >
                            <Input prefix={<UserOutlined />} placeholder="Email" onChange={(e: any) => setuserEmail(e.target.value)} />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Row >
                    <BatchFormLabel label="Password" span={24}>
                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please enter your password!' }]}
                        >
                            <Input.Password placeholder="Password" />
                        </Form.Item>
                    </BatchFormLabel>
                </Row>
                <Form.Item>
                    <Button type="primary" htmlType="submit" >
                        Log in
                    </Button>
                </Form.Item>
            </Form>
        </Card>
    )
}
export default LoginForm