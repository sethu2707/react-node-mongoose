import * as React from 'react'
import ProfileFrom from '../forms/Profile/ProfileFrom'
import { useGlobalLoadingContext } from '../../contexts/GlobalLoaderContext'
import { GetUser, updateUser } from '../../services/user.service'
import { notification } from 'antd'


interface Props { }

export interface userData {
    email?: string,
    firstName?: string,
    lastName?: string,
    gender?: string,
    dob?: any,
    profilePicture?: string,
    phoneNumber?: string,
    address?: string,
}

interface UserResonse {
    success?: boolean,
    data?: userData
}

const Profile: React.FC<Props> = () => {
    const { toggleLoadingDecorator } = useGlobalLoadingContext()
    const [respone, setResponse] = React.useState<userData>();

    React.useEffect(() => {
        toggleLoadingDecorator(true)
        getUserDetails();
    }, [])

    const getUserDetails = async () => {
        let getresponse: UserResonse = await GetUser()
        if (getresponse.success) {
            setResponse(getresponse?.data);
            toggleLoadingDecorator(false)
        } else {
            toggleLoadingDecorator(false)
        }
    }

    const onFinish = async (data: userData) => {
        toggleLoadingDecorator(true)
        let updateresponse: UserResonse = await updateUser(data)
        if (updateresponse.success) {
            setResponse(updateresponse?.data);
            openNotification('success', 'Successfully updated')
        } else {
            openErrorNotification('error', 'Failed to update your profile.Please try again later')

        }
        toggleLoadingDecorator(false)
    }

    const openNotification = (type: 'success', des: any) => {
        notification[type]({
            message: type,
            description: des,
        });
    };

    const openErrorNotification = (type: 'error', des: any) => {
        notification[type]({
            message: type,
            description: des,
        });
    };


    return (
        <div className="global-loader profile-view" >
            {respone&&<ProfileFrom onFinish={(e: userData) => onFinish(e)} res={respone} />}
        </div>
    )
}
export default Profile