import * as React from 'react'

interface State {
    hasError: boolean
}

class ErrorBoundary extends React.Component<{}, State> {
    state = { hasError: false }

    static getDerivedStateFromError(error: Error) {
        return { hasError: true }
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    }

    render() {
        if (this.state.hasError) {
            return (
                <div>
                    <div className="error-boundary">
                        <h1>Oops! Something went wrong.</h1>
                        <span>
                            Don't worry too much though, we've reported the error and will be working on it soon.
                        </span>
                    </div>
                </div>
            )
        }

        return this.props.children
    }
}

export default ErrorBoundary


