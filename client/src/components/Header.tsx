import { Layout, Typography } from 'antd'


const { Title } = Typography
const { Header: AntHeader } = Layout

const Header = () => {
    return (
        <AntHeader style={{ paddingLeft: '46px', background: '#222222' }}>
            <div style={{ display: 'flex', alignItems: 'center', height: '100%' }}>
                <Title
                    style={{
                        margin: 0,
                        color: 'white',
                        fontSize: '20px',
                        lineHeight: '24px',
                        textTransform: 'uppercase',
                        fontWeight: 800
                    }}
                >
                    User Profile
                </Title>
            </div>
        </AntHeader>
    )
}

export default Header
