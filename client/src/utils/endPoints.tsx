/**
 * @providesModule endpoints
 **/
const baseurl = "http://localhost:3000/";

const endpoints = {
    base: baseurl,
    userLogin: baseurl + 'login',
    getProfile:baseurl + 'user/',
};
export default endpoints;
