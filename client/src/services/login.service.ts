import { post } from './http.service'
import endPoints from '../utils/endPoints'

export interface CreateUserBody {
    email: string
    password: string
}

const login = async (body: CreateUserBody) => {
    try {
       const res = await post<any, { success: boolean; data?: any }>(endPoints.userLogin, body)
        return res
    } catch (e) {
        return { success: false }
    }
}


export {
    login
}
