import { get, put } from './http.service'
import endPoints from '../utils/endPoints'



const GetUser = async () => {
    let userData: any = JSON.parse(sessionStorage.getItem('user') || '')
    try {
        const header = {
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + userData?.token
        }
        const res = await get<any>(endPoints.getProfile + userData?.id, header)
        return res
    } catch (e) {
        return { success: false }
    }
}

const updateUser = async (body: any) => {
    let userData: any = JSON.parse(sessionStorage.getItem('user') || '')
    try {
        const header = {
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + userData?.token
        }
        const res = await put<any, { success: boolean; data?: any }>(endPoints.getProfile + userData?.id, body, header)
        return res
    } catch (e) {
        return { success: false }
    }
}


export {
    GetUser, updateUser
}