import 'whatwg-fetch'

export function post<S, T>(url: string = '', data?: S, header?: any): Promise<T> {
    return executeRequest(url, 'POST', data, header)
}

export function get<T>(url: string = '', header?: any): Promise<T> {
    return executeRequest(url, 'GET', '', header)
}

export function put<S, T>(url: string = '', data?: S, header?: any): Promise<T> {
    return executeRequest(url, 'PUT', data, header)
}

export function del<S, T>(url: string = '', data?: S, header?: any): Promise<T> {
    return executeRequest(url, 'DELETE', data, header)
}

export function executeRequest(url: string, type: 'GET' | 'POST' | 'PUT' | 'DELETE', data?: any, header?: any) {
    let headers = header ? header : { 'Content-Type': 'application/json' };
    let method = type;
    let body = JSON.stringify(data);
    let request = type == 'GET' ? { headers, method } : { headers, method, body }
    return fetch(url, request).then((r) => {
        return r.json()
    })
}
