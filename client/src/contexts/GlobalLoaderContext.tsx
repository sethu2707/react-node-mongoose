import * as React from 'react'

export interface GlobalLoadingContext {
    displayLoader: boolean
    toggleLoadingDecorator: (toggle: boolean) => void
}

export const GlobalLoaderContext = React.createContext<GlobalLoadingContext>({
    displayLoader: true,
    toggleLoadingDecorator: (toggle: boolean) => {}
})

export const useGlobalLoadingContext = () => React.useContext(GlobalLoaderContext)

export const GlobalLoadingDecoratorProvider: React.FC = (props) => {
    const [isLoading, setIsLoading] = React.useState<boolean>(true)


    const toggleLoader = (toggle: boolean) => {
        setIsLoading(toggle)
    }

    const globalLoadingDecoratorValue = {
        displayLoader: isLoading,
        toggleLoadingDecorator: toggleLoader
    }

    return (
        <GlobalLoaderContext.Provider value={globalLoadingDecoratorValue}>
            {props.children}
        </GlobalLoaderContext.Provider>
    )
}

