import { Application } from 'express';
import { userController } from '../Controller/userController'
import { Auth } from '../Auth/Auth'
const Authorization = new Auth();
export class userRoutes {
 constructor(public UserController:userController=new userController()){}
     public userroutes(app: Application):void {
         console.log("routes");
         app.route('/user/:id').put(Authorization.Auth,this.UserController.updateuser);
         app.route('/login').post(this.UserController.login)
         app.route('/user/:id').get(Authorization.Auth,this.UserController.getUser);
         
    }
}